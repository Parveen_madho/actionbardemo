package com.example.mani.actionmodedemo;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by mani on 28/5/16.
 */
public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ContactViewHolder>{
   ArrayList<Contact> adapter_list= new ArrayList<Contact>();
    Main2Activity mainactivity;
   Context context;

    public ContactAdapter(ArrayList<Contact> adapter_list,Context context)
    {
        this.adapter_list=adapter_list;
        this.context=context;
        mainactivity=(Main2Activity) context;

    }
    @Override
    public ContactViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_layout,parent,false);

        ContactViewHolder contactViewHolder=new ContactViewHolder(view,mainactivity);
        return contactViewHolder;
    }

    @Override
    public void onBindViewHolder(ContactViewHolder holder, int position) {

        holder.img.setImageResource(adapter_list.get(position).getImg_id());
        holder.Name.setText(adapter_list.get(position).getName());
        holder.Email.setText(adapter_list.get(position).getEmail());
        if(!mainactivity.is_in_action_mode)
        {
            holder.checkBox.setVisibility(View.GONE);

        }else
        {
            holder.checkBox.setVisibility(View.VISIBLE);
            holder.checkBox.setChecked(false);
        }

    }

    @Override
    public int getItemCount() {
        return adapter_list.size();
    }

    public static class ContactViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener
    {
        ImageView img;
        TextView Name,Email;
        CheckBox checkBox;
        Main2Activity activity;
        CardView cardView;
        public ContactViewHolder(View itemView,Main2Activity activity) {
            super(itemView);
            img=(ImageView) itemView.findViewById(R.id.img_id);
            Name=(TextView) itemView.findViewById(R.id.name);
            Email=(TextView) itemView.findViewById(R.id.email);
            checkBox=(CheckBox) itemView.findViewById(R.id.check_list_item);
            cardView=(CardView) itemView.findViewById(R.id.cardView);

            this.activity=activity;
            cardView.setOnLongClickListener(activity);
            checkBox.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
        activity.prepareSelection(v,getAdapterPosition());
        }
    }
    public void updateAdapter(ArrayList<Contact> list)
    {
        for(Contact contact:list)
        {
            adapter_list.remove(context);

        }
    notifyDataSetChanged();

    }

}
