package com.example.mani.actionmodedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity implements View.OnLongClickListener {

    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerView.Adapter adapter;
    Toolbar toolbar;
    int[] img_id = {R.drawable.image, R.drawable.image10, R.drawable.image1, R.drawable.image2, R.drawable.image3, R.drawable.image4, R.drawable.image6, R.drawable.image7, R.drawable.image8, R.drawable.image9, R.drawable.image10};
    String[] Name, Email;
    ArrayList<Contact> contacts = new ArrayList<>();
    Boolean is_in_action_mode = false;
    TextView contact_text_view;
    ArrayList<Contact> selection_list = new ArrayList<>();
    int counter = 0;
    Button delete;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        contact_text_view = (TextView) findViewById(R.id.countertext);
        contact_text_view.setVisibility(View.GONE);
        Name = getResources().getStringArray(R.array.name);
        Email = getResources().getStringArray(R.array.email);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerview);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        int i = 0;
        for (String NAME : Name) {

            Contact contact = new Contact(img_id[i], NAME, Email[i]);
            contacts.add(contact);
            i++;

        }
        adapter = new ContactAdapter(contacts, Main2Activity.this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_activity_main, menu);

        return true;
    }

    @Override
    public boolean onLongClick(View v) {
        toolbar.getMenu().clear();
        toolbar.inflateMenu(R.menu.menu_action_mode);
        contact_text_view.setVisibility(View.VISIBLE);
        is_in_action_mode = true;
        adapter.notifyDataSetChanged();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return true;
    }

    public void prepareSelection(View view, int position) {
        if (((CheckBox) view).isChecked()) {
            selection_list.add(contacts.get(position));
            counter = counter + 1;
            updateCounter(counter);
        } else {
            selection_list.remove(contacts.get(position));
            counter = counter - 1;
            updateCounter(counter);
        }
    }

    public void updateCounter(int counter) {
        if (counter == 0) {
            contact_text_view.setText("0 item Selected");

        } else {
            contact_text_view.setText(counter + "  item Selected");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.item_delete) {

            ContactAdapter contactAdapter = (ContactAdapter) adapter;
            contactAdapter.updateAdapter(selection_list);
            clearActionMode();
        }

        else if(item.getItemId()==android.R.id.home)
        {
            clearActionMode();
            adapter.notifyDataSetChanged();
        }


        return true;
    }

    public void clearActionMode() {
        is_in_action_mode = false;
        toolbar.getMenu().clear();
        toolbar.inflateMenu(R.menu.menu_activity_main);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        contact_text_view.setVisibility(View.GONE);
        contact_text_view.setText("0 item Selected");
        counter = 0;
        selection_list.clear();
    }

    @Override
    public void onBackPressed() {
        if (is_in_action_mode) {
            clearActionMode();
            adapter.notifyDataSetChanged();


        } else {

            super.onBackPressed();

        }


    }
}
